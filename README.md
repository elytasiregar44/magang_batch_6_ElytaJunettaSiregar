Git merupakan software berbasis Version Control System (VCS) yang bertugas untuk mencatat perubahan seluruh file atau repository suatu project.

Git global setup
git config --global user.name "Elyta Siregar"
git config --global user.email "elytasiregar44@gmail.com"

Create a new repository
git clone https://gitlab.com/elytasiregar44/coba.git
cd coba
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/elytasiregar44/coba.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/elytasiregar44/coba.git
git push -u origin --all
git push -u origin --tags

Make a branch
git branch "name of branch"
git checkout "name of branch"
git add .
git commit -m "message"
git push origiin "name of branch"
